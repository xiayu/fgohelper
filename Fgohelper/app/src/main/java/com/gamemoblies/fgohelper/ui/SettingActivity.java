package com.gamemoblies.fgohelper.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import com.gamemoblies.fgohelper.DataStore.AccountDataStore;
import com.gamemoblies.fgohelper.R;
import com.gamemoblies.fgohelper.Request.ContinuePrepareRequest;
import com.gamemoblies.fgohelper.ui.demo_d.DraggableExampleActivity;
import com.gamemoblies.fgohelper.util.JsonUtil;

import org.json.JSONObject;

/**
 * Created by xiayu on 2015/9/8.
 */
public class SettingActivity extends BaseActionBarActivity implements ContinuePrepareRequest.ContinuePrepareListener{

    EditText appvar;
    EditText dataVar;
    EditText conti;
    Button save;
    Button reset;
    Button conti_button;
    ContinuePrepareRequest continuePrepareRequest;
    Switch switch_button;
    Button rule;
    EditText timeout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        AccountDataStore.loadBestFollow();
        continuePrepareRequest = new ContinuePrepareRequest(this);
        appvar = (EditText) findViewById(R.id.appvar);
        dataVar = (EditText) findViewById(R.id.datavar);
        conti = (EditText) findViewById(R.id.conti);
        conti_button = (Button) findViewById(R.id.conti_button);
        switch_button = (Switch) findViewById(R.id.switch_button);
        rule = (Button) findViewById(R.id.rule);
        timeout = (EditText) findViewById(R.id.timeout);
        appvar.setText(AccountDataStore.appVer);
        dataVar.setText(AccountDataStore.datavar);
        conti.setText(AccountDataStore.ContinueCode);
        timeout.setText(AccountDataStore.TimeOut+"");
        save = (Button) findViewById(R.id.save);
        reset = (Button) findViewById(R.id.reset);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountDataStore.datavar = dataVar.getText().toString();
                AccountDataStore.appVer = appvar.getText().toString();
                AccountDataStore.TimeOut = Integer.parseInt(timeout.getText().toString());
                AccountDataStore.save();
            }
        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appvar.setText(AccountDataStore.appVer);
                dataVar.setText(AccountDataStore.datavar);
                conti.setText(AccountDataStore.ContinueCode);
            }
        });
        conti_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conti.setText("执行中");
                continuePrepareRequest.RequestData(SettingActivity.this);
            }
        });
        switch_button.setChecked(AccountDataStore.BestFollow);
        switch_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                AccountDataStore.BestFollow = isChecked;
                AccountDataStore.saveBestFollow();
            }
        });
        rule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, DraggableExampleActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void ContinueListener(String str) {
        JSONObject obj = JsonUtil.getJsonObjectFromString(str);
        if (JsonUtil.getResCode(obj).equals("00")){
            obj = JsonUtil.getJsonObjectFromJsonObject(obj, "cache");
            obj = JsonUtil.getJsonObjectFromJsonObject(obj, "updated");
            obj = JsonUtil.getJsonObjectFromJsonArray(JsonUtil.getJsonArrayFromJsonObject(obj, "userContinue"), 0);
            AccountDataStore.ContinueCode = JsonUtil.getStringFromJsonObject(obj, "continueKey");
            conti.setText(AccountDataStore.ContinueCode);
            AccountDataStore.save();
        }else{
            String msg = JsonUtil.getFailMsg(obj);
            conti.setText("error: "+msg);
        }
    }

    @Override
    public void ContinueError() {
        conti.setText("网络异常？请重试");
    }

}
