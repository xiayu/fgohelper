package com.gamemoblies.fgohelper;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class TrippleDes
{

    private static byte[] sharedkey = "b5nHjsMrqaeNliSs3jyOzgpD".getBytes();

    private static byte[] sharedvector = "wuD6keVr".getBytes();

    private static byte[] Appkey = "Tsukihime_ReMake".getBytes();

    private static byte[] Appvector = "BaldrSky".getBytes();

    public static String decrypt(String ciphertext)
    {
        Cipher c = null;
        try {
            c = Cipher.getInstance("DESede/CBC/NoPadding");
            c.init(Cipher.DECRYPT_MODE, new SecretKeySpec(sharedkey, "DESede"), new IvParameterSpec(sharedvector));
            byte[] decrypted = c.doFinal(Base64.decode(ciphertext, Base64.DEFAULT));
            return new String(decrypted, "UTF-8");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String AppDecrypt(String ciphertext)
    {
        Cipher c = null;
        try {
            c = Cipher.getInstance("DESede/CBC/NoPadding");
            c.init(Cipher.DECRYPT_MODE, new SecretKeySpec(Appkey, "DESede"), new IvParameterSpec(Appvector));
            byte[] b = Base64.decode(ciphertext, Base64.DEFAULT);
            byte[] decrypted = c.doFinal(Base64.decode(ciphertext, Base64.DEFAULT));
            return new String(decrypted, "UTF-8");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }
}
