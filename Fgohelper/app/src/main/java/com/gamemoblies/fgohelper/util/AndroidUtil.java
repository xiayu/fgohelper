package com.gamemoblies.fgohelper.util;

import android.content.Context;
import android.telephony.TelephonyManager;

/**
 * Created by xiayu on 2015/9/18.
 */
public class AndroidUtil {
    public static String GetImei(Context context){
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getDeviceId();
    }
}
