package com.gamemoblies.fgohelper.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gamemoblies.fgohelper.DataStore.AccountDataStore;
import com.gamemoblies.fgohelper.R;
import com.gamemoblies.fgohelper.Request.DownloadDropItemRequest;
import com.gamemoblies.fgohelper.Request.LoginHelperRequest;
import com.gamemoblies.fgohelper.util.JsonUtil;

import org.json.JSONObject;


/**
 * Created by xiayu on 2015/9/18.
 */
public class LoginActivity extends BaseActivity implements LoginHelperRequest.LoginListener {
    EditText editText;
    Button login;
    LoginHelperRequest loginHelperRequest;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        editText = (EditText) findViewById(R.id.userid);
        login = (Button) findViewById(R.id.login);
        loginHelperRequest = new LoginHelperRequest(this);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountDataStore.helperID = editText.getText().toString();
                loginHelperRequest.RequestData(LoginActivity.this);
                dialog = ProgressDialog.show(LoginActivity.this, null, "验证中", true, false);
            }
        });
    }

    @Override
    public void LoginSuccess(String response) {
        if (dialog.isShowing()){
            dialog.dismiss();
        }
        AccountDataStore.save();
        JSONObject object = JsonUtil.getJsonObjectFromString(response);
        String errCode = JsonUtil.getStringFromJsonObject(object, "errCode");
        String msg = JsonUtil.getStringFromJsonObject(object, "msg");
        String online = JsonUtil.getStringFromJsonObject(object, "online");
        if (online.equals("true")){
            if (errCode.equals("0")){
                Intent intent = new Intent(this, LoadingActivity.class);
                startActivity(intent);
                finish();
            }else {
                Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
            }
        }else {
            Toast.makeText(this, "服务器维护中", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void LoginError() {
        if (dialog.isShowing()){
            dialog.dismiss();
        }
        Toast.makeText(this, "网络超时请重试", Toast.LENGTH_LONG).show();
    }
}
