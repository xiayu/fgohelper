package com.gamemoblies.fgohelper.Request;

import android.util.Log;

import com.android.volley.VolleyError;
import com.gamemoblies.fgohelper.DataStore.AccountDataStore;
import com.gamemoblies.fgohelper.net.BaseListener;
import com.gamemoblies.fgohelper.net.ErrorLister;
import com.gamemoblies.fgohelper.net.FgoRequest;
import com.gamemoblies.fgohelper.net.VolleyUtil;

/**
 * Created by xiayu on 2015/10/12.
 */
public class DropItemRequest {
    public void RequestData(Object tag, String itemNum, String itemList, String questId){
        String url = "http://fgohelper.gamemoblies.com/adddropitem?itemNum=" + itemNum +
                "&itemList="+itemList+"&questId="+ questId + "&datavar=" + AccountDataStore.datavar;
        Log.i("dropitem", url);
        FgoRequest cookieRequest = new FgoRequest(url, null, new GetData(), new ErrorData());
        cookieRequest.setTag(tag);
        VolleyUtil.getRequestQueue().add(cookieRequest);
    }

    class GetData extends BaseListener {
        @Override
        public void onResponse(String response) {
            super.onResponse(response);
            Log.i("dropitem", "success");
        }
    }

    class ErrorData extends ErrorLister {
        @Override
        public void onErrorResponse(VolleyError error) {
            super.onErrorResponse(error);
            Log.i("dropitem", "failed");
        }
    }

}
