package com.gamemoblies.fgohelper.Bean;

/**
 * Created by xiayu on 2015/9/6.
 */
public class QuestBean {
    public QuestBean(){

    }

    public QuestBean(String n, String i){
        name = n;
        id = i;
        max = "3";
    }

    public QuestBean(String n, String i, String m){
        name = n;
        id = i;
        max = m;
    }
    public String name;
    public String id;
    public String max;
    public Long StartTime;
    public Long EndTime;

    public int calcLastTime(){
        long time = System.currentTimeMillis() / 1000;
        time = StartTime - time;
        return (int) time;
    }

    public String CalcNeedTimeString(){
        int t = calcLastTime();
        int hour = GetHour(t);
        int min = GetMin(t);
        int sec = GetSec(t);
        if (hour < 0 || min < 0 || sec < 0){
            hour = min = sec = 0;
        }
        if (hour > 99){
            hour = min = sec = 99;
        }
        return "倒计时："+hour+"h:"+min+"m:"+sec+"s";
    }

    private int GetHour(int t){
        return t/3600;
    }

    private int GetMin(int t){
        return t%3600/60;
    }

    private int GetSec(int t){
        return t%3600%60;
    }
}
