package com.gamemoblies.fgohelper.DataStore;

import android.content.Context;

import com.gamemoblies.fgohelper.Bean.DropItemBean;
import com.gamemoblies.fgohelper.Bean.ResultBean;
import com.gamemoblies.fgohelper.TrippleDes;
import com.gamemoblies.fgohelper.config.ConstConfig;
import com.gamemoblies.fgohelper.util.JsonUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by xiayu on 2015/10/13.
 */
public class DropItemDataStore {
    private static HashMap<String, DropItemBean> items = new HashMap<>();

    public static DropItemBean Query(String str){
        return items.get(str);
    }

    public static void init(Context context){
        try {
            FileInputStream fin = context.openFileInput(ConstConfig.DROPITEMFILENAME);
            int length = fin.available();
            byte[] bytes = new byte[length];
            fin.read(bytes);
            String json = new String(bytes, "UTF-8");
            json = TrippleDes.AppDecrypt(json);
            initItems(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void init(String json){
        initItems(json);
    }

    private static void initItems(String str) {
        JSONArray array = JsonUtil.getJSONArrayFromString(str);
        for (int i = 0; i < array.length(); i++){
            JSONObject object = JsonUtil.getJsonObjectFromString(JsonUtil.getStringFromJsonArray(array, i));
            AddItem(object);
        }
    }

    private static void AddItem(JSONObject object){
        DropItemBean bean = new DropItemBean();
        bean.QuestId = JsonUtil.getStringFromJsonObject(object, "questid");
        bean.sumline = JsonUtil.getStringFromJsonObject(object, "sumline");
        JSONObject result = JsonUtil.getJsonObjectFromJsonObject(object, "result");
        Iterator it = result.keys();
        while (it.hasNext()){
            String key = (String) it.next();
            DecimalFormat decimalFormat = new DecimalFormat(".000");
            double d = JsonUtil.getDoubleFromJsonObject(result, key, 0);
            String value;
            if (key.equals("1")){
                value = decimalFormat.format(d);
            }else {
                value = decimalFormat.format(d*100)+"%";
            }
            if (key.length() > 6){
                if (SvtDataStore.items.get(key)!=null){
                    key = SvtDataStore.items.get(key);
                }
            }else {
                if (ItemDataStore.items.get(key)!=null){
                    key = ItemDataStore.items.get(key);
                }
            }
            ResultBean temp = new ResultBean(key, value);
            bean.beans.add(temp);
        }
        items.put(bean.QuestId, bean);
    }
}
