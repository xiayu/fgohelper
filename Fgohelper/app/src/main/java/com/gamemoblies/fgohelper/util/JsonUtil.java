package com.gamemoblies.fgohelper.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Arrays;

public class JsonUtil {
    public static final String defaultIcon = "http://a.hiphotos.baidu.com/image/pic/item/77094b36acaf2eddd3ef78ee8e1001e93901939e.jpg";
    public static final String defaultNumber = "1";
    public static final String defaultString = "";
    public static final int defaultInt = 0;


    public static String getResCode(JSONObject object){
        JSONArray array = getJsonArrayFromJsonObject(object, "response");
        JSONObject obj = getJsonObjectFromJsonArray(array, 0);
        return getStringFromJsonObject(obj, "resCode");
    }

    public static String getFailMsg(JSONObject object){
        JSONArray array = getJsonArrayFromJsonObject(object, "response");
        JSONObject obj = getJsonObjectFromJsonArray(array, 0);
        obj = getJsonObjectFromJsonObject(obj, "fail");
        return getStringFromJsonObject(obj, "detail");
    }

    public static JSONObject getUserGameObject(JSONObject object){
        JSONObject obj = getJsonObjectFromJsonObject(object, "cache");
        obj = getJsonObjectFromJsonObject(obj, "replaced");
        obj = getJsonObjectFromJsonArray(getJsonArrayFromJsonObject(obj, "userGame"), 0);
        return obj;
    }

    public static JSONArray getUserItemObject(JSONObject object){
        JSONObject obj = getJsonObjectFromJsonObject(object, "cache");
        obj = getJsonObjectFromJsonObject(obj, "replaced");
        return getJsonArrayFromJsonObject(obj, "userItem");
    }

    public static JSONObject getUpdateUserGameObject(JSONObject object){
        JSONObject obj = getJsonObjectFromJsonObject(object, "cache");
        obj = getJsonObjectFromJsonObject(obj, "updated");
        obj = getJsonObjectFromJsonArray(getJsonArrayFromJsonObject(obj, "userGame"), 0);
        return obj;
    }

    public static JSONArray getUpdateUserItemObject(JSONObject object){
        JSONObject obj = getJsonObjectFromJsonObject(object, "cache");
        obj = getJsonObjectFromJsonObject(obj, "updated");
        return getJsonArrayFromJsonObject(obj, "userItem");
    }

    public static JSONObject getUserDeckObject(JSONObject object){
        JSONObject obj = getJsonObjectFromJsonObject(object, "cache");
        obj = getJsonObjectFromJsonObject(obj, "replaced");
        obj = getJsonObjectFromJsonArray(getJsonArrayFromJsonObject(obj, "userDeck"), 0);
        return obj;
    }

    public static JSONObject getFollowerInfo(JSONObject object){
        JSONObject obj = getJsonObjectFromJsonObject(object, "cache");
        obj = getJsonObjectFromJsonObject(obj, "updated");
        obj = getJsonObjectFromJsonArray(getJsonArrayFromJsonObject(obj, "userFollower"), 0);
        return getJsonObjectFromJsonArray(getJsonArrayFromJsonObject(obj, "followerInfo"), 0);
    }

    public static String getServerTime(JSONObject object){
        JSONObject obj = getJsonObjectFromJsonObject(object, "cache");
        return getStringFromJsonObject(obj, "serverTime");

    }

    public static Boolean checkErrCode(JSONObject object) throws JSONException {
		String errCode = object.getString("errCode");
		return errCode.equals("0");
	}

    public static String getStringFromJsonObject(JSONObject jsonObject, String name, String defaultValue){
        String result;
        try{
            result = jsonObject.getString(name);
            if (result.equals("null")){
                return "0";
            }
        }
        catch (Exception e){
            return defaultValue;
        }

        return result;
    }

    public static String getStringFromJsonObject(JSONObject jsonObject, String name){
        return getStringFromJsonObject(jsonObject, name, JsonUtil.defaultString);
    }

    public static boolean getBoolFromJsonObject(JSONObject jsonObject, String name){
        boolean result;
        try {
            result = jsonObject.getBoolean(name);
        } catch (JSONException e) {
            result = false;
        }
        return result;
    }

    public static boolean getBoolFromJsonObject(JSONObject jsonObject, String name, boolean defaultValue){
        boolean result;
        try {
            result = jsonObject.getBoolean(name);
        } catch (JSONException e) {
            result = defaultValue;
        }
        return result;
    }

    public static int getIntFromJsonObject(JSONObject jsonObject, String name){
        return getIntFromJsonObject(jsonObject, name, defaultInt);
    }
    
    public static int getIntFromJsonObject(JSONObject jsonObject, String name, int defaultValue){
        int result;
        try{
            result = jsonObject.getInt(name);
        }catch (Exception e){
            return defaultValue;
        }

        return result;
    }

    public static long getLongFromJsonObject(JSONObject jsonObject, String name, long defaultValue){
        long result;
        try{
            result = jsonObject.getLong(name);
        }catch (Exception e){
            return defaultValue;
        }

        return result;
    }

    public static double getDoubleFromJsonObject(JSONObject jsonObject, String name, double defaultVale) {
        double result;
        try {
            result = jsonObject.getDouble(name);
        } catch (Exception e) {
            return defaultVale;
        }

        return result;
    }
    
    public static JSONObject getJsonObjectFromJsonObject(JSONObject jsonObject, String name){
        try{
        	String temp = JsonUtil.getStringFromJsonObject(jsonObject, name);
        	JSONTokener tokener = new JSONTokener(temp);
            return  (JSONObject) tokener.nextValue();
        }catch (Exception e){
            return null;
        }
    }
    
    public static JSONObject getJsonObjectFromJsonArray(JSONArray jsonArray, int index){
        try{
            return jsonArray.getJSONObject(index);
        }catch (Exception e){
            return null;
        }
    }

    public static String getStringFromJsonArray(JSONArray jsonArray, int index){
        try{
            return jsonArray.getString(index);
        }catch (Exception e){
            return null;
        }
    }

    public static JSONArray getJsonArrayFromJsonArray(JSONArray jsonArray, int index){
        try{
            return jsonArray.getJSONArray(index);
        }catch (Exception e){
            return null;
        }
    }

    public static JSONArray getJsonArrayFromJsonObject(JSONObject jsonObject, String name){
        try{
            return jsonObject.getJSONArray(name);
        }catch (Exception e){
            return new JSONArray();
        }
    }

    public static JSONObject getJsonObjectFromString(String string){
        JSONTokener tokener = new JSONTokener(string);
        try {
            return (JSONObject)tokener.nextValue();
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public static JSONArray getJSONArrayFromString(String string){
        JSONTokener tokener = new JSONTokener(string);
        try {
            return (JSONArray)tokener.nextValue();
        }
        catch (Exception e){
            e.printStackTrace();
            return new JSONArray();
        }
    }

    public static ArrayList<String> getPictureUrls(JSONObject jsonObject, String name){
        ArrayList<String> result = new ArrayList<String>();

        String temp = getStringFromJsonObject(jsonObject, name, "");
        splitPictureUrls(temp, result);

        return result;
    }

    private static void splitPictureUrls(String allPictures, ArrayList<String> picUrls) {
        String[] picTmp = allPictures.split("\\|");
        if (!picTmp[0].equals(""))
            picUrls.addAll(Arrays.asList(picTmp));
    }

    public static JSONArray getJsonArray(JSONObject jsonObject, String name) {
        try{
            return jsonObject.getJSONArray(name);
        }
        catch (Exception e){
            e.printStackTrace();
            return new JSONArray();
        }
    }

    public static String[] getStringArrayFromJsonObject(JSONObject object, String name){
        String[] recipients;
        try {
            JSONArray temp = object.getJSONArray(name);
            int length = temp.length();
            if (length > 0) {
                recipients = new String[length];
                for (int i = 0; i < length; i++) {
                    recipients[i] = temp.getString(i);
                }
                return recipients;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return new String[0];
    }
}
