package com.gamemoblies.fgohelper;

import android.os.Bundle;
import android.widget.TextView;

import com.gamemoblies.fgohelper.DataStore.AccountDataStore;
import com.gamemoblies.fgohelper.ui.BaseActionBarActivity;

/**
 * Created by xiayu on 2015/9/12.
 */
public class UpdateNoteActivity extends BaseActionBarActivity {
    TextView textview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_note_activity);
        textview = (TextView) findViewById(R.id.update);
        if (AccountDataStore.updateNote.length() > 10){
            textview.setText(AccountDataStore.updateNote);
        }
    }
}
