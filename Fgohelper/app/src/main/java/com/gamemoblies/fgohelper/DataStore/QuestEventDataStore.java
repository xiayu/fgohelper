package com.gamemoblies.fgohelper.DataStore;

import android.content.Context;

import com.gamemoblies.fgohelper.Bean.QuestBean;
import com.gamemoblies.fgohelper.R;
import com.gamemoblies.fgohelper.util.JsonUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by xiayu on 2015/9/7.
 */
public class QuestEventDataStore {
    public static ArrayList<QuestBean> items = new ArrayList<>();

    public static void initItems(Context context){
        items.clear();
        InputStream is = context.getResources().openRawResource(R.raw.mst_quest_event);
        BufferedReader br;
        StringBuffer sb = new StringBuffer();
        sb.append("");
        String str;
        br = new BufferedReader(new InputStreamReader(is));
        try {
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        str = sb.toString();
        JSONArray array = JsonUtil.getJSONArrayFromString(str);
        items.clear();
        for (int i = 0; i < array.length(); i++){
            JSONObject object = JsonUtil.getJsonObjectFromJsonArray(array, i);
            QuestBean item = new QuestBean();
            item.name = JsonUtil.getStringFromJsonObject(object, "name");
            item.id = JsonUtil.getStringFromJsonObject(object, "id");
            item.max = "1";
            items.add(item);
        }
    }
}
