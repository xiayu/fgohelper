package com.gamemoblies.fgohelper.ui;

import android.app.Activity;

import com.umeng.analytics.MobclickAgent;

/**
 * Created by xiayu on 2015/9/3.
 */
public class BaseActivity extends Activity {
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
