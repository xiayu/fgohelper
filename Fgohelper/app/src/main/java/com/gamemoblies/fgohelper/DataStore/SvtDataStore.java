package com.gamemoblies.fgohelper.DataStore;

import android.content.Context;

import com.gamemoblies.fgohelper.R;
import com.gamemoblies.fgohelper.util.JsonUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Created by xiayu on 2015/9/3.
 */
public class SvtDataStore {
    public static HashMap<String, String> items = new HashMap<>();

    public static void initItems(Context context){
        InputStream is = context.getResources().openRawResource(R.raw.mst_svt);
        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();
        sb.append("");
        String str;
        br = new BufferedReader(new InputStreamReader(is));
        try {
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        str = sb.toString();
        JSONArray array = JsonUtil.getJSONArrayFromString(str);
        for (int i = 0; i < array.length(); i++){
            JSONObject obj = JsonUtil.getJsonObjectFromJsonArray(array, i);
            String id = JsonUtil.getStringFromJsonObject(obj, "id");
            String name = JsonUtil.getStringFromJsonObject(obj, "name");
            items.put(id, name);
        }
    }
}
