package com.gamemoblies.fgohelper.DataStore;

import com.gamemoblies.fgohelper.Bean.AccountBean;
import com.gamemoblies.fgohelper.FgoApplication;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import java.util.ArrayList;

/**
 * Created by xiayu on 2015/10/9.
 */
public class AccountManager {
    private DB snappydb;
    private static AccountManager _instance = null;
    private ArrayList<AccountBean> beans;

    public static AccountManager GetInstance(){
        if (_instance == null){
            _instance = new AccountManager();
        }
        return _instance;
    }

    private AccountManager(){
        beans = new ArrayList<>();
        initDB();
    }

    private void initDB(){
        try {
            snappydb = DBFactory.open(FgoApplication.getInstance());
            
            snappydb.close();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    public void InsertAccountObject(AccountBean bean){
        try {
            snappydb = DBFactory.open(FgoApplication.getInstance());
            snappydb.put(bean.userId, bean);
            snappydb.close();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    public void DelectAccountObject(AccountBean bean){
        try {
            snappydb = DBFactory.open(FgoApplication.getInstance());
            snappydb.del(bean.userId);
            snappydb.close();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }
}
