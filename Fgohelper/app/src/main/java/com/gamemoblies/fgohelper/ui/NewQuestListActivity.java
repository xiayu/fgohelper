package com.gamemoblies.fgohelper.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.gamemoblies.fgohelper.R;

import java.util.ArrayList;

/**
 * Created by xiayu on 2015/9/7.
 */
public class NewQuestListActivity extends BaseActionBarActivity{
    GridView gridView;

    ArrayList<QuestIndex> list = new ArrayList<>();

    private class QuestIndex{
        public String name;
        public Integer kind;
        public QuestIndex(String n, int k){
            name = n;
            kind = k;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saodang_new);
        addItem();
        gridView = (GridView) findViewById(R.id.gridview);
        GridViewAdapter adapter = new GridViewAdapter();
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(NewQuestListActivity.this, SaodangListActivity.class);
                intent.putExtra("kind", list.get(position).kind);
                startActivity(intent);
            }
        });
    }

    private void addItem(){
        list.add(new QuestIndex("周一本", 0));
        list.add(new QuestIndex("周二本", 1));
        list.add(new QuestIndex("周三本", 2));
        list.add(new QuestIndex("周四本", 3));
        list.add(new QuestIndex("周五本", 4));
        list.add(new QuestIndex("周六本", 5));
        list.add(new QuestIndex("周日本", 6));
        list.add(new QuestIndex("序章", 100));
        list.add(new QuestIndex("第一章", 101));
        list.add(new QuestIndex("第二章", 102));
        list.add(new QuestIndex("活动本", 1001));
    }

    class GridViewAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public QuestIndex getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null){
                convertView = NewQuestListActivity.this.getLayoutInflater().inflate(R.layout.quest_card, parent, false);
            }
            TextView textView = (TextView) convertView.findViewById(R.id.textview);
            textView.setText(getItem(position).name);
            return convertView;
        }
    }
}
