package com.gamemoblies.fgohelper.DataStore;

import android.content.Context;

import com.gamemoblies.fgohelper.Bean.QuestBean;
import com.gamemoblies.fgohelper.R;
import com.gamemoblies.fgohelper.util.JsonUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by xiayu on 2015/9/6.
 */
public class QuestDataStore {
    public static ArrayList<ArrayList<QuestBean>> days = new ArrayList<>();
    public static ArrayList<ArrayList<QuestBean>> free = new ArrayList<>();
    public static ArrayList<QuestBean> event = new ArrayList<>();

    public static ArrayList<Integer> index = new ArrayList<>();
    public static ArrayList<Integer> freeIndex = new ArrayList<>();
    private static void initQuestIndex(){
        index.add(R.raw.mst_quest_mon);
        index.add(R.raw.mst_quest_tues);
        index.add(R.raw.mst_quest_wed);
        index.add(R.raw.mst_quest_thur);
        index.add(R.raw.mst_quest_fri);
        index.add(R.raw.mst_quest_sat);
        index.add(R.raw.mst_quest_sun);
    }

    private static void initFreeIndex(){
        freeIndex.add(R.raw.mst_quest_free_0);
        freeIndex.add(R.raw.mst_quest_free_1);
        freeIndex.add(R.raw.mst_quest_free_2);
    }

    private static void initAllDays(Context context){
        for (int i = 0; i < index.size(); i++){
            days.add(initItems(context, index.get(i)));
        }
    }

    private static void initAllFree(Context context){
        for (int i = 0; i < freeIndex.size(); i++){
            free.add(initItems(context, freeIndex.get(i)));
        }
    }

    private static void initEvent(Context context){
        event = initItems(context, R.raw.mst_quest_event);
    }

    private static ArrayList<QuestBean> initItems(Context context, int resourceId){
        InputStream is = context.getResources().openRawResource(resourceId);
        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();
        sb.append("");
        String str;
        br = new BufferedReader(new InputStreamReader(is));
        try {
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        str = sb.toString();
        JSONArray array = JsonUtil.getJSONArrayFromString(str);
        ArrayList<QuestBean> beans = new ArrayList<>();
        for (int i = 0; i < array.length(); i++){
            JSONObject object = JsonUtil.getJsonObjectFromJsonArray(array, i);
            QuestBean item = new QuestBean();
            item.name = JsonUtil.getStringFromJsonObject(object, "name");
            item.id = JsonUtil.getStringFromJsonObject(object, "id");
            item.max = JsonUtil.getStringFromJsonObject(object, "afterClear");
            beans.add(item);
        }
        return beans;
    }

    public static void initItems(Context context){
        initQuestIndex();
        initAllDays(context);
        initFreeIndex();
        initAllFree(context);
        //initEvent(context);
    }
}
