package com.gamemoblies.fgohelper.Request;

import com.android.volley.Request;
import com.gamemoblies.fgohelper.DataStore.AccountDataStore;
import com.gamemoblies.fgohelper.net.BaseListener;
import com.gamemoblies.fgohelper.net.CookieRequest;
import com.gamemoblies.fgohelper.net.ErrorLister;
import com.gamemoblies.fgohelper.net.VolleyUtil;

/**
 * Created by xiayu on 2015/8/24.
 */
public class TopGameDataRequest  {
    public interface TopGameDataListener{
        public void GetData(String obj);
    }

    public TopGameDataRequest(TopGameDataListener l){
        listener = l;
    }

    private TopGameDataListener listener = null;

    public void RequestData(Object tag){
        String url = AccountDataStore.baseUrl + "gamedata/top?_userId=" + AccountDataStore.userId;
        CookieRequest cookieRequest = new CookieRequest(Request.Method.POST,url, null, new GetData(), new ErrorLister());
        cookieRequest.setTag(tag);
        cookieRequest.setDefaultCookie();
        VolleyUtil.getRequestQueue().add(cookieRequest);
    }

    public class GetData extends BaseListener{
        @Override
        public void onResponse(String response) {
            super.onResponse(response);
            listener.GetData(response);
        }
    }
}
