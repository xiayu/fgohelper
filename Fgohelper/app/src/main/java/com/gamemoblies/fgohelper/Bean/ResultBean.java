package com.gamemoblies.fgohelper.Bean;

/**
 * Created by xiayu on 2015/10/13.
 */
public class ResultBean{
    public ResultBean(String n, String p){
        name = n;
        percent = p;
    }
    public String name;
    public String percent;
}