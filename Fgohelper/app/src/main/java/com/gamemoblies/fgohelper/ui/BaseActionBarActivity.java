package com.gamemoblies.fgohelper.ui;

import android.support.v7.app.ActionBarActivity;

import com.umeng.analytics.MobclickAgent;

/**
 * Created by xiayu on 2015/9/3.
 */
public class BaseActionBarActivity extends ActionBarActivity {
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
