package com.gamemoblies.fgohelper.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.gamemoblies.fgohelper.DataStore.DropItemDataStore;
import com.gamemoblies.fgohelper.DataStore.FriendRuleDataStore;
import com.gamemoblies.fgohelper.DataStore.MstExpDataStore;
import com.gamemoblies.fgohelper.DataStore.QuestTimeDataStore;
import com.gamemoblies.fgohelper.Request.DownloadDropItemRequest;
import com.umeng.analytics.MobclickAgent;
import com.gamemoblies.fgohelper.DataStore.AccountDataStore;
import com.gamemoblies.fgohelper.DataStore.ItemDataStore;
import com.gamemoblies.fgohelper.DataStore.PlayerInfo;
import com.gamemoblies.fgohelper.DataStore.QuestDataStore;
import com.gamemoblies.fgohelper.DataStore.QuestEventDataStore;
import com.gamemoblies.fgohelper.DataStore.SvtDataStore;
import com.gamemoblies.fgohelper.R;
import com.gamemoblies.fgohelper.Request.LoginHelperRequest;
import com.gamemoblies.fgohelper.TrippleDes;
import com.gamemoblies.fgohelper.util.JsonUtil;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;

import cn.bmob.v3.Bmob;

/**
 * Created by xiayu on 2015/9/3.
 */
public class LoadingActivity extends BaseActivity implements LoginHelperRequest.LoginListener,
        DownloadDropItemRequest.DropItemListener{
    String id = "64ccf831191a9036eb5eb60f3633594a";
    LoginHelperRequest loginHelperRequest;
    DownloadDropItemRequest downloadDropItemRequest;

    public void initAccountDataStore(String datavar, String appvar){
        AccountDataStore.load();
        AccountDataStore.loadBestFollow();
        AccountDataStore.setDatavar(datavar);
        AccountDataStore.setAppVer(appvar);
        AccountDataStore.updateNote = MobclickAgent.getConfigParams(this, "updateNote");
        PlayerInfo.setBattleLog(MobclickAgent.getConfigParams(this, "log"));
        AccountDataStore.save();
    }

    private void initAccount(){
        GetRoot();
        String temp = CopySaveFile();
        String json = TrippleDes.decrypt(temp);
        AccountDataStore.init(json);
    }

    public void initFGOData(){
        ItemDataStore.initItems(this);
        SvtDataStore.initItems(this);
        QuestDataStore.initItems(this);
        QuestEventDataStore.initItems(this);
        QuestTimeDataStore.initItems(this);
        MstExpDataStore.init(this);
        AccountDataStore.load();
        FriendRuleDataStore.loadRule();
    }

    public void initUMeng(){
        MobclickAgent.setCatchUncaughtExceptions(true);
        MobclickAgent.updateOnlineConfig(this);
        Bmob.initialize(this, id);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lily_activity);
        initUMeng();
        initAccount();
        initFGOData();
        loginHelperRequest = new LoginHelperRequest(this);
        downloadDropItemRequest = new DownloadDropItemRequest(this);
        if (AccountDataStore.helperID.equals("")){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }else {
            loginHelperRequest.RequestData(this);
        }
    }

    public String CopySaveFile(){
        File file = getFileStreamPath("authsave.dat");
        String temp = "";
        if (file.exists()){
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(file));
                String tempString;
                while ((tempString = reader.readLine()) != null) {
                    temp += tempString;
                }
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e1) {
                    }
                }
            }
        }
        return temp;
    }

    private void GetRoot(){
        Process p;
        try {
            // Preform su to get root privledges
            p = Runtime.getRuntime().exec("su");
            OutputStream os = p.getOutputStream();
            DataOutputStream dos = new DataOutputStream(os);
            dos.writeBytes("cat /data/data/com.aniplex.fategrandorder/files/authsave.dat > /data/data/com.gamemoblies.fgohelper/files/authsave.dat" + "\n");
            dos.writeBytes("chmod 777 /data/data/com.gamemoblies.fgohelper/files/authsave.dat"  + "\n");
            dos.writeBytes("exit\n");
            dos.flush();

            // Attempt to write a file to a root-only
            try {
                p.waitFor();
                if (p.exitValue() != 255) {
                    // TODO Code to run on success
                    Toast.makeText(this, "root", Toast.LENGTH_SHORT);
                }
                else {
                    // TODO Code to run on unsuccessful
                    Toast.makeText(this, "need root", Toast.LENGTH_SHORT).show();
                }
            } catch (InterruptedException e) {
                // TODO Code to run in interrupted exception
                Toast.makeText(this, "need root", Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            // TODO Code to run in input/output exception
            Toast.makeText(this, "need root", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void LoginSuccess(String response) {
        JSONObject object = JsonUtil.getJsonObjectFromString(response);
        String errCode = JsonUtil.getStringFromJsonObject(object, "errCode");
        String msg = JsonUtil.getStringFromJsonObject(object, "msg");
        String online = JsonUtil.getStringFromJsonObject(object, "online");
        String datavar = JsonUtil.getStringFromJsonObject(object, "datavar");
        String appvar = JsonUtil.getStringFromJsonObject(object, "appvar");
        String appDataVar = JsonUtil.getStringFromJsonObject(object, "appDataVar");
        initAccountDataStore(datavar, appvar);
        if (online.equals("true")){
            if (errCode.equals("0")){
                if (appDataVar.equals(AccountDataStore.appDataVar)){
                    DropItemDataStore.init(this);
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(this, "下载掉落数据", Toast.LENGTH_LONG).show();
                    AccountDataStore.appDataVar = appDataVar;
                    downloadDropItemRequest.RequestData(this);
                }
            }else {
                Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }else {
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void LoginError() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void DropItemSuccess() {
        AccountDataStore.save();
        Toast.makeText(this, "下载掉落数据成功", Toast.LENGTH_LONG).show();
        DropItemDataStore.init(this);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void DropItemError() {
        Toast.makeText(this, "下载掉落数据失败", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
