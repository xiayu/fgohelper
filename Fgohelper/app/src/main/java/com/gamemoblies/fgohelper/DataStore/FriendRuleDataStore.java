package com.gamemoblies.fgohelper.DataStore;

import android.content.Context;
import android.content.SharedPreferences;

import com.gamemoblies.fgohelper.FgoApplication;
import com.gamemoblies.fgohelper.R;
import com.gamemoblies.fgohelper.util.JsonUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by xiayu on 2015/10/22.
 */
public class FriendRuleDataStore {
    public static ArrayList<String> rule = new ArrayList<>();

    public static void saveRule(){
        String temp="";
        for (int i = 0; i < rule.size(); i++){
            temp = temp + rule.get(i) + ",";
        }
        SharedPreferences setting = FgoApplication.getInstance().getSharedPreferences("Setting", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = setting.edit();
        editor.putString("rule", temp);
        editor.commit();
    }

    public static void loadRule(){
        SharedPreferences setting = FgoApplication.getInstance().getSharedPreferences("Setting", Context.MODE_PRIVATE);
        String temp  = setting.getString("rule", "");
        rule.clear();
        if (temp.equals("")){
            loadSvtInfo();
        }else {
            String[] t = temp.split(",");
            for (int i = 0; i < t.length; i++){
                if (!t[i].equals("")){
                    rule.add(t[i]);
                }
            }
        }
    }

    public static void resetFriendRule(){
        SharedPreferences setting = FgoApplication.getInstance().getSharedPreferences("Setting", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = setting.edit();
        editor.putString("rule", "");
        editor.commit();
        loadSvtInfo();
    }

    private static void loadSvtInfo(){
        InputStream is = FgoApplication.getInstance().getResources().openRawResource(R.raw.friend_rule_info);
        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();
        sb.append("");
        String str;
        br = new BufferedReader(new InputStreamReader(is));
        try {
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        str = sb.toString();
        JSONArray array = JsonUtil.getJSONArrayFromString(str);
        rule.clear();
        for (int i = 0; i < array.length(); i++){
            JSONObject obj = JsonUtil.getJsonObjectFromJsonArray(array, i);
            String id = JsonUtil.getStringFromJsonObject(obj, "id");
            rule.add(id);
        }
    }
}
