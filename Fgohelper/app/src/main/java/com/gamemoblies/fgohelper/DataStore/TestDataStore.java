package com.gamemoblies.fgohelper.DataStore;

import android.content.Context;
import android.util.Log;

import com.gamemoblies.fgohelper.TrippleDes;
import com.jayway.jsonpath.JsonPath;
import com.gamemoblies.fgohelper.R;
import com.gamemoblies.fgohelper.util.JsonUtil;

import net.minidev.json.JSONUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xiayu on 2015/9/3.
 */
public class TestDataStore {
    public static void test(Context context){
        InputStream is = context.getResources().openRawResource(R.raw.dropitem_des);
        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();
        sb.append("");
        String str;
        br = new BufferedReader(new InputStreamReader(is));
        try {
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String json = TrippleDes.AppDecrypt(sb.toString());
        DropItemDataStore.init(json);
    }

    public static String GetDoubleFromJson(String str){
        net.minidev.json.JSONArray array = JsonPath.read(str, "$..userSvtLeaderHash");
        String jsonString = array.toJSONString();
        JSONArray jsonArray = JsonUtil.getJSONArrayFromString(jsonString);
        return FindDouble(jsonArray);
    }

    public static String GetUserIdFromJson(String str){
        net.minidev.json.JSONArray array = JsonPath.read(str, "$..userSvtLeaderHash");
        String jsonString = array.toJSONString();
        JSONArray jsonArray = JsonUtil.getJSONArrayFromString(jsonString);
        return FindID(jsonArray);
    }

    private static String FindDouble(JSONArray array){
        String id = FindIDFromArray(array, "9400440");
        if (id != null){
            return id;
        }
        return "0";
    }

    private static String FindID(JSONArray array){
        String id;
        ArrayList<String> rules = FriendRuleDataStore.rule;
        for (int i = 0; i < rules.size(); i++){
            id = FindIDFromArray(array, rules.get(i));
            if (id != null){
                return id;
            }
        }
        JSONObject object = JsonUtil.getJsonObjectFromJsonArray(array, 1);
        id = JsonUtil.getStringFromJsonObject(object, "userId");
        return id;
    }

    private static String FindIDFromArray(JSONArray array, String id){
        for (int i = 0; i < array.length(); i++){
            JSONObject object = JsonUtil.getJsonObjectFromJsonArray(array, i);
            String sid = JsonUtil.getStringFromJsonObject(object, "userId");
            JSONObject equipTarget1 = JsonUtil.getJsonObjectFromJsonObject(object, "equipTarget1");
            if (equipTarget1 == null){
                continue;
            }
            String svtId = JsonUtil.getStringFromJsonObject(equipTarget1, "svtId");
            if (svtId.equals(id)){
                return sid;
            }
        }
        return null;
    }

    public static class DropItem {
        public String id="";
        public String num="";
        public String name="";
    }
}
