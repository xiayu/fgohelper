package com.gamemoblies.fgohelper.Request;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.gamemoblies.fgohelper.DataStore.AccountDataStore;
import com.gamemoblies.fgohelper.net.BaseListener;
import com.gamemoblies.fgohelper.net.CookieRequest;
import com.gamemoblies.fgohelper.net.ErrorLister;
import com.gamemoblies.fgohelper.net.VolleyUtil;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by xiayu on 2015/9/12.
 */
public class FullAPRequest {
    public interface FullAPListener{
        public void ApListener(String str);
        public void ApError();
    }

    private FullAPListener listener;

    public FullAPRequest(FullAPListener l){
        listener = l;
    }

    public void RequestData(Object tag){
        String url = AccountDataStore.baseUrl + "shop/purchaseByStone?_userId=" + AccountDataStore.userId;
        CookieRequest cookieRequest = new CookieRequest(Request.Method.POST,url, null, new GetData(), new ErrorData()){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                TreeMap<String, String> map = (TreeMap<String, String>) super.getParams();
                map.put("id", "2");
                map.put("num", "1");
                map.put("authCode", GetAuthCode(map));
                return map;
            }
        };
        cookieRequest.setTag(tag);
        cookieRequest.setDefaultCookie();
        cookieRequest.setRetryPolicy(new DefaultRetryPolicy(AccountDataStore.TimeOut * 1000, 0, 1.0f));
        VolleyUtil.getRequestQueue().add(cookieRequest);
    }

    public class GetData extends BaseListener {
        @Override
        public void onResponse(String response) {
            super.onResponse(response);
            listener.ApListener(response);
        }
    }

    public class ErrorData extends ErrorLister {
        @Override
        public void onErrorResponse(VolleyError error) {
            super.onErrorResponse(error);
            listener.ApError();
        }
    }
}
