package com.gamemoblies.fgohelper.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.gamemoblies.fgohelper.Bean.QuestBean;
import com.gamemoblies.fgohelper.DataStore.QuestTimeDataStore;
import com.gamemoblies.fgohelper.ui.demo_d.DraggableExampleActivity;
import com.umeng.analytics.MobclickAgent;
import com.umeng.fb.FeedbackAgent;
import com.umeng.update.UmengUpdateAgent;
import com.gamemoblies.fgohelper.DataStore.AccountDataStore;
import com.gamemoblies.fgohelper.DataStore.PlayerInfo;
import com.gamemoblies.fgohelper.DonateActivity;
import com.gamemoblies.fgohelper.R;
import com.gamemoblies.fgohelper.Request.TopGameDataRequest;
import com.gamemoblies.fgohelper.Request.TopLoginRequest;
import com.gamemoblies.fgohelper.UpdateNoteActivity;
import com.gamemoblies.fgohelper.net.VolleyUtil;
import com.gamemoblies.fgohelper.util.JsonUtil;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends BaseActionBarActivity implements TopGameDataRequest.TopGameDataListener,
        TopLoginRequest.TopLoginListener{

    TextView test;
    TextView userId;
    TextView authKey;
    TextView secretKey;
    TextView np;
    TextView freeStore;
    TextView activeDeckId;
    TextView cookie;
    TextView userName;
    TextView lv;
    TextView qp;
    TopLoginRequest topLoginRequest;
    Button test1;
    Button saodang;
    Button feedback;
    Button donateButton;
    Button update_note;
    FeedbackAgent agent;

    TextView RandomName;
    Button RandomButton;
    Button RandomList;
    TextView RandomTime;
    View RandomLayout;

    QuestBean bean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ConfigUI();
        getFilesDir();
        test.setText("获得帐号信息中");
        userId.setText(AccountDataStore.userId);
        authKey.setText(AccountDataStore.authKey);
        secretKey.setText(AccountDataStore.secretKey);
        topLoginRequest = new TopLoginRequest(this);
        topLoginRequest.RequestData(this);
        MobclickAgent.updateOnlineConfig(this);
        UmengUpdateAgent.update(this);
        agent = new FeedbackAgent(MainActivity.this);
        agent.setWelcomeInfo("欢迎提交bug，以及希望添加功能，有时间会回复喵~");
        agent.sync();
    }

    private void configRandomTask() {
        RandomLayout = findViewById(R.id.RandomLayout);
        RandomName = (TextView) findViewById(R.id.randomName);
        RandomButton = (Button) findViewById(R.id.random);
        RandomTime = (TextView) findViewById(R.id.randomTime);
        RandomList = (Button) findViewById(R.id.randomList);
        bean = QuestTimeDataStore.findNextItem();
        if (bean == null){
            RandomLayout.setVisibility(View.GONE);
        }else{
            RandomName.setText("随机本:" + bean.name);
            RandomList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (AccountDataStore.cookie.equals("")) {
                        Toast.makeText(MainActivity.this, "获取登入信息中", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Intent intent = new Intent(MainActivity.this, QuestTimeListActivity.class);
                    startActivity(intent);
                }
            });
            RandomButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (AccountDataStore.cookie.equals("")) {
                        Toast.makeText(MainActivity.this, "获取登入信息中", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Intent intent = new Intent(MainActivity.this, SaodangActivity.class);
                    intent.putExtra("id", bean.id);
                    intent.putExtra("max", bean.max);
                    startActivity(intent);
                }
            });
            RandomTime.setText(bean.CalcNeedTimeString());
        }
    }

    public void ConfigUI(){
        donateButton = (Button) findViewById(R.id.button);
        test = (TextView) findViewById(R.id.test);
        userId = (TextView) findViewById(R.id.userId);
        authKey = (TextView) findViewById(R.id.authKey);
        secretKey = (TextView) findViewById(R.id.secretKey);
        np = (TextView) findViewById(R.id.np);
        lv = (TextView) findViewById(R.id.lv);
        freeStore = (TextView) findViewById(R.id.freestore);
        activeDeckId = (TextView) findViewById(R.id.activeDeckId);
        cookie = (TextView) findViewById(R.id.cookie);
        userName = (TextView) findViewById(R.id.userName);
        test1 = (Button) findViewById(R.id.test1);
        saodang = (Button) findViewById(R.id.saodang);
        qp = (TextView) findViewById(R.id.qp);
        update_note = (Button) findViewById(R.id.update_note);
        feedback = (Button) findViewById(R.id.feedback);
        test1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                test.setText("获得帐号信息中");
                topLoginRequest.RequestData(this);
            }
        });
        saodang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AccountDataStore.cookie.equals("")){
                    Toast.makeText(MainActivity.this, "获取登入信息中", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(MainActivity.this, NewQuestListActivity.class);
                startActivity(intent);
            }
        });
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agent.startFeedbackActivity();
            }
        });
        donateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DonateActivity.class);
                startActivity(intent);
            }
        });
        update_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, UpdateNoteActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void GetData(String obj) {
        test.setText(obj.toString());
        Log.e("error top", obj);
    }

    @Override
    public void GetLoginData(String str) {
        JSONObject obj = JsonUtil.getJsonObjectFromString(str);
        if (JsonUtil.getResCode(obj).equals("00")){
            test.setText("success");
            PlayerInfo.calcNowNp(obj);
            PlayerInfo.calcNowApple(obj);
            PlayerInfo.getActiveDeckId(obj);
            userName.setText(PlayerInfo.name+"");
            np.setText(PlayerInfo.np+"");
            lv.setText(PlayerInfo.lv+"");
            freeStore.setText(PlayerInfo.freestore+"");
            activeDeckId.setText(PlayerInfo.activeDeckId+"");
            cookie.setText(AccountDataStore.cookie);
            qp.setText(PlayerInfo.appleNum);
        }else{
            String msg = JsonUtil.getFailMsg(obj);
            test.setText("error: "+msg);
        }
    }

    @Override
    public void LoginError() {
        test.setText("网络异常？请重试");
    }

    @Override
    public void onPause() {
        super.onPause();
        VolleyUtil.getRequestQueue().cancelAll(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        configRandomTask();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){

        }else{

        }
    }

    private void setTimer(){
        RandomTime.setText(bean.CalcNeedTimeString());
    }

    static class MyHandler extends Handler {
        WeakReference<MainActivity> mActivityReference;

        MyHandler(MainActivity activity) {
            mActivityReference= new WeakReference<MainActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            final MainActivity activity = mActivityReference.get();
            if (activity != null) {
                activity.setTimer();
            }
        }
    }
}
