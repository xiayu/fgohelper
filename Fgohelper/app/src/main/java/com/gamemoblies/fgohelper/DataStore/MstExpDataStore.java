package com.gamemoblies.fgohelper.DataStore;

import android.content.Context;

import com.gamemoblies.fgohelper.R;
import com.gamemoblies.fgohelper.util.JsonUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by xiayu on 2015/9/22.
 */
public class MstExpDataStore {
    private static ArrayList<Integer> exps = new ArrayList<>();

    public static int GetNextExp(){
        return GetNextExp(PlayerInfo.lv, PlayerInfo.exp);
    }

    public static int GetNextExp(int lv, int e){
        if (lv > exps.size()){
            return 999999999;
        }else{
            int nexp = exps.get(lv-1);
            return nexp - e;
        }
    }

    public static void init(Context context){
        InputStream is = context.getResources().openRawResource(R.raw.mst_user_exp);
        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();
        sb.append("");
        String str;
        br = new BufferedReader(new InputStreamReader(is));
        try {
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        str = sb.toString();
        JSONArray array = JsonUtil.getJSONArrayFromString(str);
        for (int i = 0; i < array.length(); i++){
            JSONObject obj = JsonUtil.getJsonObjectFromJsonArray(array, i);
            int e = JsonUtil.getIntFromJsonObject(obj, "exp");
            exps.add(e);
        }
    }
}
