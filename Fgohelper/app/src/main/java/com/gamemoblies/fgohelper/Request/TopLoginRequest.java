package com.gamemoblies.fgohelper.Request;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.gamemoblies.fgohelper.DataStore.AccountDataStore;
import com.gamemoblies.fgohelper.net.BaseListener;
import com.gamemoblies.fgohelper.net.CookieRequest;
import com.gamemoblies.fgohelper.net.ErrorLister;
import com.gamemoblies.fgohelper.net.VolleyUtil;

/**
 * Created by xiayu on 2015/8/25.
 */
public class TopLoginRequest {
    public interface TopLoginListener{
        public void GetLoginData(String str);
        public void LoginError();
    }

    private TopLoginListener listener;

    public TopLoginRequest(TopLoginListener l){
        listener = l;
    }

    public void RequestData(Object tag){
        String url = AccountDataStore.baseUrl + "login/top?_userId=" + AccountDataStore.userId;
        CookieRequest cookieRequest = new CookieRequest(Request.Method.POST,url, null, new GetData(), new ErrorData());
        cookieRequest.setTag(tag);
        cookieRequest.setRetryPolicy(new DefaultRetryPolicy(AccountDataStore.TimeOut * 1000, 0, 1.0f));
        VolleyUtil.getRequestQueue().add(cookieRequest);
    }

    public class GetData extends BaseListener {
        @Override
        public void onResponse(String response) {
            super.onResponse(response);
            listener.GetLoginData(response);
        }
    }

    public class ErrorData extends ErrorLister{
        @Override
        public void onErrorResponse(VolleyError error) {
            super.onErrorResponse(error);
            listener.LoginError();
        }
    }
}
