package com.gamemoblies.fgohelper.Request;

import com.android.volley.VolleyError;
import com.gamemoblies.fgohelper.DataStore.AccountDataStore;
import com.gamemoblies.fgohelper.FgoApplication;
import com.gamemoblies.fgohelper.net.BaseListener;
import com.gamemoblies.fgohelper.net.CookieRequest;
import com.gamemoblies.fgohelper.net.ErrorLister;
import com.gamemoblies.fgohelper.net.FgoRequest;
import com.gamemoblies.fgohelper.net.VolleyUtil;
import com.gamemoblies.fgohelper.util.AndroidUtil;

/**
 * Created by xiayu on 2015/9/18.
 */
public class LoginHelperRequest {
    public interface LoginListener{
        void LoginSuccess(String response);
        void LoginError();
    }

    private LoginListener listener;

    public LoginHelperRequest(LoginListener l){
        listener = l;
    }

    public void RequestData(Object tag){
        String url = "http://fgohelper.gamemoblies.com/foo?id=" + AccountDataStore.helperID +
                "&uid="+AccountDataStore.userId+"&imei="+ AndroidUtil.GetImei(FgoApplication.getInstance());
        FgoRequest cookieRequest = new FgoRequest(url, null, new GetData(), new ErrorData());
        cookieRequest.setTag(tag);
        VolleyUtil.getRequestQueue().add(cookieRequest);
    }

    class GetData extends BaseListener{
        @Override
        public void onResponse(String response) {
            super.onResponse(response);
            listener.LoginSuccess(response);
        }
    }

    class ErrorData extends ErrorLister{
        @Override
        public void onErrorResponse(VolleyError error) {
            super.onErrorResponse(error);
            listener.LoginError();
        }
    }
}
