package com.gamemoblies.fgohelper.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;

import com.gamemoblies.fgohelper.Bean.DropItemBean;
import com.gamemoblies.fgohelper.Bean.ResultBean;
import com.gamemoblies.fgohelper.DataStore.AccountDataStore;
import com.gamemoblies.fgohelper.DataStore.DropItemDataStore;
import com.gamemoblies.fgohelper.DataStore.MstExpDataStore;
import com.gamemoblies.fgohelper.DataStore.TestDataStore;
import com.gamemoblies.fgohelper.Request.DropItemRequest;
import com.gamemoblies.fgohelper.Request.UseItemRequest;
import com.jayway.jsonpath.JsonPath;
import com.gamemoblies.fgohelper.Bean.DropItemObject;
import com.gamemoblies.fgohelper.Bean.ErrorBean;
import com.gamemoblies.fgohelper.DataStore.ItemDataStore;
import com.gamemoblies.fgohelper.DataStore.PlayerInfo;
import com.gamemoblies.fgohelper.DataStore.SvtDataStore;
import com.gamemoblies.fgohelper.R;
import com.gamemoblies.fgohelper.Request.BattleResultRequest;
import com.gamemoblies.fgohelper.Request.BattleSetupRequest;
import com.gamemoblies.fgohelper.Request.FollowListRequest;
import com.gamemoblies.fgohelper.Request.FullAPRequest;
import com.gamemoblies.fgohelper.net.VolleyUtil;
import com.gamemoblies.fgohelper.util.JsonUtil;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xiayu on 2015/8/29.
 */
public class SaodangActivity extends BaseActionBarActivity implements
        BattleSetupRequest.BattleSetupListener,
        BattleResultRequest.BattleResultListener,
        FollowListRequest.FollowListListener,
        FullAPRequest.FullAPListener,
        UseItemRequest.UseItemListener{
    Button start, end, apfull, apple;
    TextView log;
    TextView np;
    TextView qp;
    TextView exp;
    TextView freeStore;
    String id;
    BattleSetupRequest setupRequest;
    BattleResultRequest resultRequest;
    DropItemRequest dropItemRequest;
    FollowListRequest followListRequest;
    FullAPRequest fullAPRequest;
    UseItemRequest useItemRequest;
    ArrayList<DropItem> drops = new ArrayList<>();
    String url1;
    String url2;
    String log1;
    String max;
    TableRow dropName;
    TableRow dropPercent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saodang);
        id = getIntent().getStringExtra("id");
        max = getIntent().getStringExtra("max");
        initUI();
        setDropInfo();
        setUserInfo();
        setupRequest = new BattleSetupRequest(this, id);
        resultRequest = new BattleResultRequest(this);
        followListRequest = new FollowListRequest(this);
        fullAPRequest = new FullAPRequest(this);
        useItemRequest = new UseItemRequest(this);
        dropItemRequest = new DropItemRequest();
    }

    private void initUI(){
        start = (Button) findViewById(R.id.start);
        end = (Button) findViewById(R.id.end);
        apfull = (Button) findViewById(R.id.apfull);
        apple = (Button) findViewById(R.id.apple);
        log = (TextView) findViewById(R.id.log);
        np = (TextView) findViewById(R.id.np);
        qp = (TextView) findViewById(R.id.qp);
        exp = (TextView)findViewById(R.id.exp);
        freeStore = (TextView) findViewById(R.id.freestore);
        dropName = (TableRow) findViewById(R.id.dropName);
        dropPercent = (TableRow) findViewById(R.id.dropPercent);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setClickable(false);
                end.setClickable(true);
                log.setText("执行中");
                followListRequest.RequestData(this);
            }
        });
        end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setClickable(false);
                start.setClickable(true);
                log.setText("执行中");
                url2 = "battleId=" + AccountDataStore.loadLastBattleId() + "&1" + "&BattleLog=" + PlayerInfo.BattleLog;
                resultRequest.RequestData(SaodangActivity.this, AccountDataStore.loadLastBattleId(), "1", "", PlayerInfo.BattleLog);
            }
        });
        apfull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                log.setText("执行中");
                fullAPRequest.RequestData(SaodangActivity.this);
            }
        });
        apple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                log.setText("执行中");
                useItemRequest.RequestData(SaodangActivity.this);
            }
        });
    }

    private void setDropInfo(){
        DropItemBean bean = DropItemDataStore.Query(id);
        TextView n = new TextView(this);
        TextView p = new TextView(this);
        n.setBackgroundResource(R.drawable.table_bg);
        p.setBackgroundResource(R.drawable.table_bg);
        n.setText("样本量");
        if (bean == null){
            p.setText("0");
            dropName.addView(n);
            dropPercent.addView(p);
            return;
        }
        p.setText(bean.sumline);
        dropName.addView(n);
        dropPercent.addView(p);
        for (int i = 0; i < bean.beans.size(); i++){
            ResultBean b = bean.beans.get(i);
            n = new TextView(this);
            p = new TextView(this);
            n.setBackgroundResource(R.drawable.table_bg);
            p.setBackgroundResource(R.drawable.table_bg);
            n.setText(b.name);
            p.setText(b.percent);
            dropName.addView(n);
            dropPercent.addView(p);
        }
    }

    private void setUserInfo(){
        np.setText("ap："+PlayerInfo.np);
        qp.setText("苹果: "+PlayerInfo.appleNum);
        freeStore.setText("石头：" + PlayerInfo.freestore);
        exp.setText("所需exp:" + MstExpDataStore.GetNextExp());
    }

    @Override
    public void GetSetupListener(String str) {
        JSONObject obj = JsonUtil.getJsonObjectFromString(str);
        if (JsonUtil.getResCode(obj).equals("00")) {
            setBattleId(str);
            String temp = getDropList(str);
            if (temp.equals("")){
                log.setText("没有道具");
            }else
                log.setText(temp);
            PlayerInfo.calUpdateNp(obj);
            setUserInfo();
        }else{
            String msg = JsonUtil.getFailMsg(obj);
            log.setText("error: " + msg);
        }
    }

    @Override
    public void SetupError() {
        start.setClickable(true);
        log.setText("网络异常？请重试");
    }

    @Override
    public void GetResultListener(String str) {
        JSONObject obj = JsonUtil.getJsonObjectFromString(str);
        if (JsonUtil.getResCode(obj).equals("00")) {
            log.setText("success");
            PlayerInfo.calUpdateNp(obj);
            setUserInfo();
        }else{
            String msg = JsonUtil.getFailMsg(obj);
            log.setText("error: "+msg);
            log1 = msg;
            ErrorBean bean = new ErrorBean();
            bean.log = log1;
            bean.url1 = url1;
            bean.url2 = url2;
            bean.nickName = PlayerInfo.name;
            bean.save(this);
        }
    }

    @Override
    public void ResultError() {
        end.setClickable(true);
        log.setText("网络异常？请重试");
    }

    private void setBattleId(String str){
        JSONObject obj = JsonUtil.getJsonObjectFromString(str);
        obj = JsonUtil.getJsonObjectFromJsonObject(obj, "cache");
        obj = JsonUtil.getJsonObjectFromJsonObject(obj, "replaced");
        obj = JsonUtil.getJsonObjectFromJsonArray(JsonUtil.getJsonArrayFromJsonObject(obj, "battle"), 0);
        AccountDataStore.setLastBattleId(JsonUtil.getStringFromJsonObject(obj, "id"));
    }

    private String ListToString(List<Integer> list){
        String temp = "";
        for (int i = 0; i < list.size(); i++){
            temp = temp + list.get(i).toString() + ",";
        }
        return temp;
    }

    private String getDropList(String str){
        try{
            drops.clear();
            JSONObject object = JsonUtil.getJsonObjectFromString(str);
            str = object.toString();
            List<Integer> dropInfos = JsonPath.read(str, "$..objectId");
            List<Integer> nums = JsonPath.read(str, "$..num");
            DropItemObject dropItemObject = new DropItemObject();
            dropItemObject.questId = id;
            dropItemObject.itemList = ListToString(dropInfos);
            dropItemObject.itemNum = ListToString(nums);
            dropItemObject.save(this);
            dropItemRequest.RequestData("upload", ListToString(nums), ListToString(dropInfos), id);
            for (int i = 0; i < dropInfos.size(); i++){
                DropItem item = new DropItem();
                item.id = dropInfos.get(i).toString();
                item.num = nums.get(i).toString();
                if (item.id.length() > 6){
                    if (SvtDataStore.items.get(item.id)!=null){
                        item.name = SvtDataStore.items.get(item.id);
                    }
                }else {
                    if (ItemDataStore.items.get(item.id)!=null){
                        item.name = ItemDataStore.items.get(item.id);
                    }
                }
                drops.add(item);
            }
            String result="";
            for (int i = 0; i < drops.size(); i++){
                DropItem item = drops.get(i);
                result = result + item.name + "×" + item.num + "\n";
            }
            return result;
        }catch (Exception e){
            return "道具列表解析异常，不影响扫荡进行";
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        VolleyUtil.getRequestQueue().cancelAll(this);
    }

    @Override
    public void FollowListener(String str) {
        JSONObject obj = JsonUtil.getJsonObjectFromString(str);
        if (JsonUtil.getResCode(obj).equals("00")) {
            String userid;
            if (AccountDataStore.BestFollow){
                userid = TestDataStore.GetUserIdFromJson(str);
            }else {
                List<Integer> userId = JsonPath.read(str, "$..userId");
                userid = userId.get(1).toString();
            }
            url1 = "id=" + id + "&3" + "&activeDeckId=" +  PlayerInfo.activeDeckId + "&followerId=" + userid;
            setupRequest.RequestData(SaodangActivity.this, id, max, PlayerInfo.activeDeckId, userid);
        }else {
            start.setClickable(true);
            String msg = JsonUtil.getFailMsg(obj);
            log.setText("error: "+msg);
        }
    }

    @Override
    public void FollowError() {
        start.setClickable(true);
        log.setText("网络异常？请重试");
    }

    @Override
    public void ApListener(String str) {
        JSONObject obj = JsonUtil.getJsonObjectFromString(str);
        if (JsonUtil.getResCode(obj).equals("00")) {
            PlayerInfo.calUpdateNp(obj);
            log.setText("success");
            setUserInfo();
        }else{
            String msg = JsonUtil.getFailMsg(obj);
            log.setText("error: "+msg);
        }
    }

    @Override
    public void ApError() {
        log.setText("网络异常？请重试");
    }


    @Override
    public void UseItemData(String str) {
        JSONObject obj = JsonUtil.getJsonObjectFromString(str);
        if (JsonUtil.getResCode(obj).equals("00")) {
            PlayerInfo.calUpdateNp(obj);
            PlayerInfo.calcUpdateApple(obj);
            log.setText("success");
            setUserInfo();
        }else{
            String msg = JsonUtil.getFailMsg(obj);
            log.setText("error: "+msg);
        }
    }

    @Override
    public void UseItemError() {
        log.setText("网络异常？请重试");
    }

    public class DropItem {
        public String id;
        public String num;
        public String name;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
