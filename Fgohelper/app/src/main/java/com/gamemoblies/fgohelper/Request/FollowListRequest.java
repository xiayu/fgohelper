package com.gamemoblies.fgohelper.Request;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.gamemoblies.fgohelper.DataStore.AccountDataStore;
import com.gamemoblies.fgohelper.net.BaseListener;
import com.gamemoblies.fgohelper.net.CookieRequest;
import com.gamemoblies.fgohelper.net.ErrorLister;
import com.gamemoblies.fgohelper.net.VolleyUtil;

/**
 * Created by xiayu on 2015/9/7.
 */
public class FollowListRequest {
    public interface FollowListListener{
        public void FollowListener(String str);
        public void FollowError();
    }

    private FollowListListener listener;

    public FollowListRequest(FollowListListener l){
        listener = l;
    }

    public void RequestData(Object tag){
        String url = AccountDataStore.baseUrl + "follower/list?_userId=" + AccountDataStore.userId;
        CookieRequest cookieRequest = new CookieRequest(Request.Method.POST,url, null, new GetData(), new ErrorData());
        cookieRequest.setTag(tag);
        cookieRequest.setDefaultCookie();
        cookieRequest.setRetryPolicy(new DefaultRetryPolicy(AccountDataStore.TimeOut * 1000, 0, 1.0f));
        VolleyUtil.getRequestQueue().add(cookieRequest);
    }

    public class GetData extends BaseListener {
        @Override
        public void onResponse(String response) {
            super.onResponse(response);
            listener.FollowListener(response);
        }
    }

    public class ErrorData extends ErrorLister {
        @Override
        public void onErrorResponse(VolleyError error) {
            super.onErrorResponse(error);
            listener.FollowError();
        }
    }
}
