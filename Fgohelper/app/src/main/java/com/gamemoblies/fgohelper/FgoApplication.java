package com.gamemoblies.fgohelper;

import android.app.Application;

/**
 * Created by cloudywei on 2015/8/21.
 */
public class FgoApplication extends Application {
    private static FgoApplication instance;

    public static FgoApplication getInstance(){ return instance; }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
