package com.gamemoblies.fgohelper.DataStore;

import android.content.Context;

import com.gamemoblies.fgohelper.Bean.QuestBean;
import com.gamemoblies.fgohelper.R;
import com.gamemoblies.fgohelper.util.JsonUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by cloudywei on 2015/10/21.
 */
public class QuestTimeDataStore {
    public static ArrayList<QuestBean> items = new ArrayList<>();
    public static Comparator<QuestBean> comparator = new Comparator<QuestBean>() {
        @Override
        public int compare(QuestBean lhs, QuestBean rhs) {
            return (int) (lhs.StartTime - rhs.StartTime);
        }
    };

    public static int findNextItemIndex(){
        long time = System.currentTimeMillis() / 1000;
        for (int i = 0; i < items.size(); i++){
            QuestBean bean = items.get(i);
            if (time > bean.StartTime && time < bean.EndTime || time < bean.StartTime){
                return i;
            }
        }
        return -1;
    }

    public static ArrayList<QuestBean> findNextItems(){
        int t = findNextItemIndex();
        ArrayList<QuestBean> beans = new ArrayList<>();
        if (t != -1){
            for (int i = t; i < items.size(); i++){
                beans.add(items.get(i));
            }
        }
        return beans;
    }

    public static QuestBean findNextItem(){
        int t = findNextItemIndex();
        if (t == -1){
            return null;
        }
        return items.get(t);
    }

    public static void initItems(Context context){
        InputStream is = context.getResources().openRawResource(R.raw.mst_quest_time);
        BufferedReader br = null;
        StringBuffer sb = new StringBuffer();
        sb.append("");
        String str;
        br = new BufferedReader(new InputStreamReader(is));
        try {
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        str = sb.toString();
        JSONArray array = JsonUtil.getJSONArrayFromString(str);
        items.clear();
        for (int i = 0; i < array.length(); i++){
            JSONObject object = JsonUtil.getJsonObjectFromJsonArray(array, i);
            QuestBean item = new QuestBean();
            item.name = JsonUtil.getStringFromJsonObject(object, "name");
            item.id = JsonUtil.getStringFromJsonObject(object, "id");
            item.max = "1";
            item.StartTime = JsonUtil.getLongFromJsonObject(object, "openedAt", 0);
            item.EndTime = JsonUtil.getLongFromJsonObject(object, "closedAt", 0);
            items.add(item);
        }
        Collections.sort(items, comparator);
    }
}
