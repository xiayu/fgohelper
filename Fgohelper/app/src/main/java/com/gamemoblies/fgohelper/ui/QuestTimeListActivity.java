package com.gamemoblies.fgohelper.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.gamemoblies.fgohelper.Bean.QuestBean;
import com.gamemoblies.fgohelper.DataStore.QuestTimeDataStore;
import com.gamemoblies.fgohelper.R;

import java.util.ArrayList;

/**
 * Created by xiayu on 2015/10/22.
 */
public class QuestTimeListActivity extends BaseActionBarActivity {
    ListView listView;

    ArrayList<QuestBean> items = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saodang_list);
        items = QuestTimeDataStore.findNextItems();
        listView = (ListView) findViewById(R.id.list);
        SaodangAdapter adapter = new SaodangAdapter();
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(QuestTimeListActivity.this, SaodangActivity.class);
                QuestBean item = items.get(position);
                intent.putExtra("id", item.id);
                intent.putExtra("max", item.max);
                startActivity(intent);
            }
        });
    }

    public class SaodangAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public QuestBean getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null){
                convertView = LayoutInflater.from(QuestTimeListActivity.this).inflate(R.layout.questtime_item, parent,
                        false);
            }
            QuestBean item = getItem(position);
            TextView textView = (TextView) convertView.findViewById(R.id.textview);
            textView.setText(item.name);
            TextView time = (TextView) convertView.findViewById(R.id.time);
            time.setText(""+item.CalcNeedTimeString());
            return convertView;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
