package com.gamemoblies.fgohelper.net;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.gamemoblies.fgohelper.FgoApplication;
import com.gamemoblies.fgohelper.util.JsonUtil;

import org.json.JSONArray;
import org.json.JSONObject;

public class VolleyUtil {
    public static RequestQueue requestQueue = Volley.newRequestQueue(FgoApplication.getInstance());

    public static RequestQueue getRequestQueue(){
        return requestQueue;
    }

	public static JSONArray getArrayResultFromJsonObject(JSONObject object){
		JSONArray result = JsonUtil.getJsonArray(object, "result");
		return result;
	}

    public static JSONObject getObjectResultFromJsonObject(JSONObject object) {
        JSONObject result = JsonUtil.getJsonObjectFromJsonObject(object, "result");
        return result;
    }

    public static String getStringResultFromJsonObject(JSONObject object) {
        String result = JsonUtil.getStringFromJsonObject(object, "result");
        return result;
    }

    public static int getIntResultFromJsonObject(JSONObject object) {
        int result = JsonUtil.getIntFromJsonObject(object, "result", 0);
        return result;
    }
	
	public static int getErrorCode(JSONObject object){
		int errorCode = JsonUtil.getIntFromJsonObject(object, "errCode");
		return errorCode;
	}
	
	public static String getMsg(JSONObject object){
		String msg = JsonUtil.getStringFromJsonObject(object, "msg");
		return msg;
	}
}
