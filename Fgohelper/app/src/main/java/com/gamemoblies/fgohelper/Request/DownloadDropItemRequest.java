package com.gamemoblies.fgohelper.Request;

import android.content.Context;
import android.util.Log;

import com.android.volley.VolleyError;
import com.gamemoblies.fgohelper.DataStore.AccountDataStore;
import com.gamemoblies.fgohelper.FgoApplication;
import com.gamemoblies.fgohelper.config.ConstConfig;
import com.gamemoblies.fgohelper.net.BaseListener;
import com.gamemoblies.fgohelper.net.ErrorLister;
import com.gamemoblies.fgohelper.net.FgoRequest;
import com.gamemoblies.fgohelper.net.VolleyUtil;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by xiayu on 2015/10/12.
 */
public class DownloadDropItemRequest {
    public interface DropItemListener{
        void DropItemSuccess();
        void DropItemError();
    }

    private DropItemListener listener;

    public DownloadDropItemRequest(DropItemListener l){
        listener = l;
    }

    public void RequestData(Object tag){
        String url = "http://fgohelper.gamemoblies.com/dropitem.json";
        FgoRequest cookieRequest = new FgoRequest(url, null, new GetData(), new ErrorData());
        cookieRequest.setTag(tag);
        VolleyUtil.getRequestQueue().add(cookieRequest);
    }

    class GetData extends BaseListener {
        @Override
        public void onResponse(String response) {
            super.onResponse(response);
            Log.i("dropitem", "success");
            writeDropItemFile(response);
            listener.DropItemSuccess();
        }
    }

    class ErrorData extends ErrorLister {
        @Override
        public void onErrorResponse(VolleyError error) {
            super.onErrorResponse(error);
            Log.i("dropitem", "failed");
            listener.DropItemError();
        }
    }

    private void writeDropItemFile(String str){
        try {
            FileOutputStream fout = FgoApplication.getInstance().openFileOutput(ConstConfig.DROPITEMFILENAME, Context.MODE_PRIVATE);
            fout.write(str.getBytes());
            fout.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
