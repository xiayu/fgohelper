package com.gamemoblies.fgohelper.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.gamemoblies.fgohelper.Bean.QuestBean;
import com.gamemoblies.fgohelper.DataStore.QuestDataStore;
import com.gamemoblies.fgohelper.DataStore.QuestEventDataStore;
import com.gamemoblies.fgohelper.R;

import java.util.ArrayList;

/**
 * Created by xiayu on 2015/8/31.
 */
public class SaodangListActivity extends BaseActionBarActivity {
    ListView listView;

    ArrayList<QuestBean> items = new ArrayList<>();
    int kind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saodang_list);
        kind = getIntent().getIntExtra("kind", 0);
        AddItemsForDataStore();
        listView = (ListView) findViewById(R.id.list);
        SaodangAdapter adapter = new SaodangAdapter();
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(SaodangListActivity.this, SaodangActivity.class);
                QuestBean item = items.get(position);
                intent.putExtra("id", item.id);
                intent.putExtra("max", item.max);
                startActivity(intent);
            }
        });
    }

    public class SaodangAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public QuestBean getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null){
                convertView = LayoutInflater.from(SaodangListActivity.this).inflate(R.layout.saodang_item, parent,
                        false);
            }
            QuestBean item = getItem(position);
            TextView textView = (TextView) convertView.findViewById(R.id.textview);
            textView.setText(item.name);
            return convertView;
        }
    }

    private void AddItemsForDataStore(){
        if (kind < 100){
            items = QuestDataStore.days.get(kind);
        }else if (kind >= 100 && kind < 1000){
            items = QuestDataStore.free.get(kind-100);
        }else if (kind == 1001){
            items = QuestEventDataStore.items;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
