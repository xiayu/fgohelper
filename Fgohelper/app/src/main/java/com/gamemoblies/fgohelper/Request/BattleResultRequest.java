package com.gamemoblies.fgohelper.Request;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.gamemoblies.fgohelper.DataStore.AccountDataStore;
import com.gamemoblies.fgohelper.net.BaseListener;
import com.gamemoblies.fgohelper.net.CookieRequest;
import com.gamemoblies.fgohelper.net.ErrorLister;
import com.gamemoblies.fgohelper.net.VolleyUtil;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by xiayu on 2015/8/25.
 */
public class BattleResultRequest {
    public interface BattleResultListener{
        public void GetResultListener(String str);
        public void ResultError();
    }

    private BattleResultListener listener;

    public BattleResultRequest(BattleResultListener l){
        listener = l;
    }

    public void RequestData(Object tag, final String battleId, final String battleResult, final String scores, final String action){
        String url = AccountDataStore.baseUrl + "battle/result?_userId=" + AccountDataStore.userId;
        CookieRequest cookieRequest = new CookieRequest(Request.Method.POST,url, null, new GetData(), new ErrorData()){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                TreeMap<String, String> map = (TreeMap<String, String>) super.getParams();
                map.put("battleId", battleId);
                map.put("battleResult", battleResult);
                map.put("scores", scores);
                map.put("action", action);
                map.put("authCode", GetAuthCode(map));
                return map;
            }
        };
        cookieRequest.setDefaultCookie();
        cookieRequest.setTag(tag);
        cookieRequest.setRetryPolicy(new DefaultRetryPolicy(AccountDataStore.TimeOut * 1000, 0, 1.0f));
        VolleyUtil.getRequestQueue().add(cookieRequest);
    }

    public class GetData extends BaseListener {
        @Override
        public void onResponse(String response) {
            super.onResponse(response);
            listener.GetResultListener(response);
        }
    }

    public class ErrorData extends ErrorLister{
        @Override
        public void onErrorResponse(VolleyError error) {
            super.onErrorResponse(error);
            listener.ResultError();
        }
    }
}
