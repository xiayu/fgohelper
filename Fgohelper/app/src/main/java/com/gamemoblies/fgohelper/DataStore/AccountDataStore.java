package com.gamemoblies.fgohelper.DataStore;

import android.content.Context;
import android.content.SharedPreferences;

import com.gamemoblies.fgohelper.Bean.AccountBean;
import com.gamemoblies.fgohelper.FgoApplication;
import com.gamemoblies.fgohelper.util.JsonUtil;

import org.json.JSONObject;

/**
 * Created by cloudywei on 2015/8/21.
 */

public class AccountDataStore {
    public static String userName = null;

    public static String userId = null;

    public static String authKey = null;

    public static String secretKey = null;

    public static String datavar = "33";

    public static String appDataVar = "-1000";

    public static String appVer = "1.0.4";

    public static String ContinueCode = "";

    public static String updateNote = "";

    public static String helperID = "";

    private static String LastBattleId = "";

    public static boolean BestFollow = true;

    public static int TimeOut = 60;

    public AccountBean getAccountBean(String name){
        AccountBean bean = new AccountBean();
        bean.name = name;
        bean.userId = userId;
        bean.authKey = authKey;
        bean.secretKey = secretKey;
        return bean;
    }

    public void setAccountBean(AccountBean bean){
        userName = bean.name;
        userId = bean.userId;
        authKey = bean.authKey;
        secretKey = bean.secretKey;
    }

    public static void setDatavar(String str){
        if (str.length() > 0){
            if (Integer.parseInt(datavar) < Integer.parseInt(str))
                datavar = str;
        }
    }

    public static void setAppVer(String str){
        if (str.length() > 0){
            appVer = str;
        }
    }

    public static String cookie = "";

    public static String baseUrl = "http://game.fate-go.jp/";

    public static void init(String str){
        JSONObject jsonObject = JsonUtil.getJsonObjectFromString(str);
        userId = JsonUtil.getStringFromJsonObject(jsonObject, "userId");
        authKey = JsonUtil.getStringFromJsonObject(jsonObject, "authKey");
        secretKey = JsonUtil.getStringFromJsonObject(jsonObject, "secretKey");
    }

    public static void save(){
        SharedPreferences setting = FgoApplication.getInstance().getSharedPreferences("Setting", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = setting.edit();
        editor.putString("appVar", appVer);
        editor.putString("datavar", datavar);
        editor.putString("code", ContinueCode);
        editor.putString("helperID", helperID);
        editor.putString("appDataVar", appDataVar);
        editor.putInt("TimeOut", TimeOut);
        editor.commit();
    }

    public static void load(){
        SharedPreferences setting = FgoApplication.getInstance().getSharedPreferences("Setting", Context.MODE_PRIVATE);
        setAppVer(setting.getString("appVar", ""));
        setDatavar(setting.getString("datavar", ""));
        ContinueCode = setting.getString("code", "");
        helperID = setting.getString("helperID", "");
        TimeOut = setting.getInt("TimeOut", 60);
        appDataVar = setting.getString("appDataVar", "-1000");
    }

    public static void setLastBattleId(String str){
        SharedPreferences setting = FgoApplication.getInstance().getSharedPreferences("Setting", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = setting.edit();
        LastBattleId = str;
        editor.putString("LastBattleId", LastBattleId);
        editor.commit();
    }

    public static String loadLastBattleId(){
        SharedPreferences setting = FgoApplication.getInstance().getSharedPreferences("Setting", Context.MODE_PRIVATE);
        return setting.getString("LastBattleId", "");
    }

    public static void saveBestFollow(){
        SharedPreferences setting = FgoApplication.getInstance().getSharedPreferences("Setting", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = setting.edit();
        editor.putBoolean("BestFollow", BestFollow);
        editor.commit();
    }

    public static void loadBestFollow(){
        SharedPreferences setting = FgoApplication.getInstance().getSharedPreferences("Setting", Context.MODE_PRIVATE);
        BestFollow = setting.getBoolean("BestFollow", true);
    }
}
