package com.gamemoblies.fgohelper.Request;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.gamemoblies.fgohelper.DataStore.AccountDataStore;
import com.gamemoblies.fgohelper.net.BaseListener;
import com.gamemoblies.fgohelper.net.CookieRequest;
import com.gamemoblies.fgohelper.net.ErrorLister;
import com.gamemoblies.fgohelper.net.VolleyUtil;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by xiayu on 2015/8/25.
 */
public class BattleSetupRequest {
    public interface BattleSetupListener{
        public void GetSetupListener(String str);
        public void SetupError();
    }

    public BattleSetupListener listener;
    String battleId;

    public BattleSetupRequest(BattleSetupListener l, String id){
        listener = l;
        battleId = id;
    }

    public void RequestData(Object tag, final String questId, final String questPhase, final String activeDeckId, final String followerId){
        String url = AccountDataStore.baseUrl + "battle/setup?_userId=" + AccountDataStore.userId;
        CookieRequest cookieRequest = new CookieRequest(Request.Method.POST,url, null, new GetData(), new ErrorData()){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                TreeMap<String, String> map = (TreeMap<String, String>) super.getParams();
                map.put("questId", questId);
                map.put("questPhase", questPhase);
                map.put("activeDeckId", activeDeckId);
                map.put("followerId", followerId);
                map.put("authCode", GetAuthCode(map));
                return map;
            }
        };
        cookieRequest.setRetryPolicy(new DefaultRetryPolicy(AccountDataStore.TimeOut * 1000, 0, 1.0f));
        cookieRequest.setDefaultCookie();
        cookieRequest.setTag(tag);
        VolleyUtil.getRequestQueue().add(cookieRequest);
    }

    public class GetData extends BaseListener {
        @Override
        public void onResponse(String response) {
            super.onResponse(response);
            listener.GetSetupListener(response);
        }
    }

    public class ErrorData extends ErrorLister{
        @Override
        public void onErrorResponse(VolleyError error) {
            super.onErrorResponse(error);
            listener.SetupError();
        }
    }
}
