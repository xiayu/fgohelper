package com.gamemoblies.fgohelper.net;

import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.gamemoblies.fgohelper.DataStore.AccountDataStore;

import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;

public class CookieRequest extends StringRequest {

    //public static string UAHeader = "Mozilla/5.0 (iPhone; CPU iPhone OS 8_1_3 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12B466";

    public static String UAHeader = "Dalvik/1.6.0 (Linux; U; Android 4.4.2; HONOR H30-L02 Build/HonorH30-L02)";

    private Map<String, String> mHeaders = new HashMap<String, String>();

    public CookieRequest(String url, JSONObject jsonRequest,
                         Listener<String> listener, ErrorListener errorListener) {
        super(url, listener, errorListener);
        // TODO Auto-generated constructor stub
    }

    public CookieRequest(int method, String url, JSONObject jsonRequest,
                         Listener<String> listener, ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        Map<String, String> responseHeaders = response.headers;
        String rawCookies = responseHeaders.get("Set-Cookie");
        if (rawCookies != null){
            AccountDataStore.cookie = rawCookies;
        }
        return Response.success(getRealString(response.data), HttpHeaderParser.parseCacheHeaders(response));
    }

    public void setDefaultCookie() {
        String cookie = AccountDataStore.cookie;
        mHeaders.put("Cookie", cookie);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        // add "User-Agent" for server identify
        mHeaders.put("User-Agent", UAHeader);
        mHeaders.put("X-Unity-Version", "4.6.7p1");
        mHeaders.put("Accept-Encoding", "gzip");
        return mHeaders;
    }

    public void setTimeoutAndRetries(int timeoutMs, int retries) {
        this.setRetryPolicy(new DefaultRetryPolicy(
                timeoutMs,
                retries,
                1.0f));
    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        TreeMap<String, String> map = new TreeMap<>();
        map.put("appVer", AccountDataStore.appVer);
        map.put("userId", AccountDataStore.userId);
        map.put("authKey", AccountDataStore.authKey);
        map.put("dataVer", AccountDataStore.datavar);
        map.put("lastAccessTime", System.currentTimeMillis() / 1000 + "");
        map.put("authCode", GetAuthCode(map));
        return map;
    }

    public static String GetAuthCode(Map<String, String> headers) {
        String text = "";
        for (Map.Entry<String, String> current : headers.entrySet()) {
            if (!text.equals("")) {
                String text2 = text;
                text = text2 + "&" + current.getKey() + "=" + current.getValue();
            } else {
                text = text + current.getKey() + "=" + current.getValue();
            }
        }
        text = text + ":" + AccountDataStore.secretKey;
        String hash = Base64.encodeToString(DigestUtils.sha(text), Base64.NO_WRAP);
        Log.e("error", hash);
        return hash;
    }

    private int getShort(byte[] data) {
        return (int) ((data[0] << 8) | data[1] & 0xFF);
    }

    private String getRealString(byte[] data) {
        byte[] h = new byte[2];
        h[0] = (data)[0];
        h[1] = (data)[1];
        int head = getShort(h);
        boolean t = head == 0x1f8b;
        InputStream in;
        StringBuilder sb = new StringBuilder();
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(data);
            if (t) {
                in = new GZIPInputStream(bis);
            } else {
                in = bis;
            }
            BufferedReader r = new BufferedReader(new InputStreamReader(in), 1000);
            for (String line = r.readLine(); line != null; line = r.readLine()) {
                sb.append(line);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
