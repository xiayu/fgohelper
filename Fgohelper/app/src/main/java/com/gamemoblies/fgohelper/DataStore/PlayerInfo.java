package com.gamemoblies.fgohelper.DataStore;

import com.gamemoblies.fgohelper.util.JsonUtil;


import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by xiayu on 2015/8/26.
 */
public class PlayerInfo {
    public static int np;
    public static int freestore;
    public static String activeDeckId;
    public static String name = "";
    public static int lv = 0;
    public static String qp = "";
    public static int exp = 0;
    public static String BattleLog="{ logs:[{\\\"uid\\\":1,\\\"ty\\\":2},{\\\"uid\\\":1,\\\"ty\\\":3},{\\\"uid\\\":1,\\\"ty\\\":5},{\\\"uid\\\":1,\\\"ty\\\":1},{\\\"uid\\\":1,\\\"ty\\\":1},{\\\"uid\\\":1,\\\"ty\\\":2},{\\\"uid\\\":1,\\\"ty\\\":2},{\\\"uid\\\":1,\\\"ty\\\":1},{\\\"uid\\\":1,\\\"ty\\\":3}]}";
    public static String appleNum = "";

    public static int time = 300;

    public static void setBattleLog(String str){
        if (str.length() > 10){
            BattleLog = str;
        }
    }

    public static void calcAppleNum(JSONArray userItem){
        for (int i = 0; i < userItem.length(); i++){
            JSONObject item = JsonUtil.getJsonObjectFromJsonArray(userItem, i);
            String id = JsonUtil.getStringFromJsonObject(item, "itemId");
            if (id.equals("100")){
                appleNum = JsonUtil.getStringFromJsonObject(item, "num");
                return;
            }
        }
        appleNum = "0";
    }

    public static void calcUserInfo(JSONObject userGame, String serverTime){
        name = JsonUtil.getStringFromJsonObject(userGame, "name");
        lv = JsonUtil.getIntFromJsonObject(userGame, "lv");
        exp = JsonUtil.getIntFromJsonObject(userGame, "exp");
        qp = JsonUtil.getStringFromJsonObject(userGame, "qp");
        String actRecoverAt = JsonUtil.getStringFromJsonObject(userGame, "actRecoverAt");
        int actMax = JsonUtil.getIntFromJsonObject(userGame, "actMax");
        np = actMax - (Integer.parseInt(actRecoverAt) - Integer.parseInt(serverTime))/time - 1;
        if (np > actMax){
            np = actMax;
        }
        if (np < 0){
            np = 0;
        }
        freestore = JsonUtil.getIntFromJsonObject(userGame, "stone");
    }

    public static void calcNowNp(JSONObject obj){
        String serverTime = JsonUtil.getServerTime(obj);
        JSONObject userGame = JsonUtil.getUserGameObject(obj);
        calcUserInfo(userGame, serverTime);
    }

    public static void calcNowApple(JSONObject obj){
        calcAppleNum(JsonUtil.getUserItemObject(obj));
    }

    public static void calcUpdateApple(JSONObject obj){
        calcAppleNum(JsonUtil.getUpdateUserItemObject(obj));
    }

    public static void calUpdateNp(JSONObject obj){
        String serverTime = JsonUtil.getServerTime(obj);
        JSONObject userGame = JsonUtil.getUpdateUserGameObject(obj);
        calcUserInfo(userGame, serverTime);
    }

    public static void getActiveDeckId(JSONObject obj){
        JSONObject userDeck = JsonUtil.getUserDeckObject(obj);
        activeDeckId = JsonUtil.getStringFromJsonObject(userDeck, "id");
    }
}
