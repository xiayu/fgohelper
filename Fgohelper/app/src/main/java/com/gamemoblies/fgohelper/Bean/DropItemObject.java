package com.gamemoblies.fgohelper.Bean;

import cn.bmob.v3.BmobObject;

/**
 * Created by xiayu on 2015/9/7.
 */
public class DropItemObject extends BmobObject {
    public String questId;
    public String itemList;
    public String itemNum;
}
