package com.gamemoblies.fgohelper.net;

import android.util.Log;

import com.android.volley.Response.ErrorListener;
import com.android.volley.VolleyError;

public class ErrorLister implements ErrorListener {
	String url = "";

	public ErrorLister() {
		super();
	}

	public ErrorLister(String url) {
		super();
		this.url = url;
	}

	@Override
	public void onErrorResponse(VolleyError error) {
        if (error != null && error.getMessage()!=null){
            Log.e("error", error.getMessage());
        }
	}
}
